package fr.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.afpa.beans.Adresse;
import fr.afpa.services.DBConnection;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AdresseDAO {

	/**
	 * Pour ajouter une novelle adresse � la base de donn�es
	 * 
	 * @param adresse
	 * @return la meme adresse pass�e en param avec son id gener�
	 */
	public Adresse addAdresse(Adresse adresse) {
		Connection con = DBConnection.getConnection();
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement("INSERT INTO Adresse (numero,libelle,cp,ville) values (?,?,?,?)",
						Statement.RETURN_GENERATED_KEYS);
				pstm.setString(1, adresse.getNumero());
				pstm.setString(2, adresse.getLibelle());
				pstm.setString(3, adresse.getCp());
				pstm.setString(4, adresse.getVille());
				pstm.executeUpdate();
				ResultSet res = pstm.getGeneratedKeys();

				if (res.next()) {
					adresse.setIdAdresse(res.getInt("id_Adresse"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return adresse;
	}

	/**
	 * Pour mettre � jour l'adresse dans la base de donn�es
	 * 
	 * @param adresse
	 */
	public void updateAdresse(Adresse adresse) {
		Connection con = DBConnection.getConnection();
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement(
						"UPDATE Adresse set numero = ?,libelle = ? ,cp = ?,ville = ? WHERE id_Adresse = ?");
				pstm.setString(1, adresse.getNumero());
				pstm.setString(2, adresse.getLibelle());
				pstm.setString(3, adresse.getCp());
				pstm.setString(4, adresse.getVille());
				pstm.setInt(5, adresse.getIdAdresse());
				pstm.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * Pour recuperer une adresse de la base de donn�es en fonction de son id
	 * 
	 * @param idAdresse
	 * @return
	 */
	public Adresse searchAdresse(int idAdresse) {
		Connection con = DBConnection.getConnection();
		Adresse adresse = null;
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement(
						"SELECT id_Adresse,numero,libelle,cp,ville FROM Adresse WHERE id_Adresse = ?");
				pstm.setInt(1, idAdresse);
				ResultSet res = pstm.executeQuery();

				if (res.next()) {
					adresse = new Adresse(res.getInt(1), res.getString(2), res.getString(3), res.getString(4),
							res.getString(5));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return adresse;
	}

}
