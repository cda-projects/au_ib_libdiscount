package fr.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.beans.Annonce;
import fr.afpa.services.DBConnection;

/**
 * 
 * @author CDA
 *
 */
public class AnnonceDAO {

	/**
	 * Pour ajouter une annonce � la base de donn�es
	 * 
	 * @param annonce
	 * @return
	 */
	public void addAnnonce(Annonce annonce) {
		Connection con = DBConnection.getConnection();
		if (con != null) {
			PreparedStatement pstm = null;
			try {
				pstm = con.prepareStatement("INSERT INTO annonce (id_User," 
																+ "titre," 
																+ "isbn," 
																+ "date_edition,"
																+ "maison_edition," 
																+ "prix_unitaire," 
																+ "quantite," 
																+ "remise)" 
																+ " values (?,?,?,?,?,?,?,?)");

				pstm.setInt(1, annonce.getIdUser());
				pstm.setString(2, annonce.getTitre());
				pstm.setString(3, annonce.getIsbn());
				pstm.setDate(4, annonce.getDateEdition());
				pstm.setString(5, annonce.getMaisonEdition());
				pstm.setFloat(6, annonce.getPrixUnitaire());
				pstm.setInt(7, annonce.getQuantite());
				pstm.setFloat(8, annonce.getRemise());

				pstm.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * Pour mettre � jour les infos d'une annonce dans la base de donn�es
	 * 
	 * @param annonce
	 */
	public void updateAnnonce(Annonce annonce) {
		Connection con = DBConnection.getConnection();
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement(
						"UPDATE Annonce set titre = ?," 
										  + "isbn = ? ," 
										  + "maison_edition = ?," 
										  + "date_edition = ?,"
										  + "prix_unitaire = ?," 
										  + "quantite = ?," 
										  + "remise = ? " 
										  + "WHERE id_Annonce = ?");

				pstm.setString(1, annonce.getTitre());
				pstm.setString(2, annonce.getIsbn());
				pstm.setString(3, annonce.getMaisonEdition());
				pstm.setDate(4, annonce.getDateEdition());
				pstm.setFloat(5, annonce.getPrixUnitaire());
				pstm.setInt(6, annonce.getQuantite());
				pstm.setFloat(7, annonce.getRemise());
				pstm.setInt(8, annonce.getIdAnnonce());

				pstm.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * Pour supprimer une annonce de la base de donn�es en fonction de son
	 * id_annonce
	 * 
	 * @param idAnnonce
	 */
	public void supprimerAnnonce(int idAnnonce) {
		Connection con = DBConnection.getConnection();
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement("DELETE FROM annonce WHERE id_annonce = ? ");
				pstm.setInt(1, idAnnonce);
				pstm.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	 * Pour lister toutes les annonces enregistr�es dans la base de donn�es
	 * 
	 * @return
	 */
	public List<Annonce> listerAnnonce() {
		Connection con = DBConnection.getConnection();
		Statement pstm = null;
		List<Annonce> listAnnonce = new ArrayList<>();
		if (con != null) {

			try {
				pstm = con.createStatement();
				String query = "select * from annonce";
				ResultSet res = pstm.executeQuery(query);
				listAnnonce = resultSetToList(res);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return listAnnonce;
	}

	/**
	 * Pour les toutes les annonce d'un utilisateur en fonction de son id
	 * 
	 * @param idUser
	 * @return
	 */
	public List<Annonce> listerMesAnnonce(int idUser) {
		Connection con = DBConnection.getConnection();
		PreparedStatement pstm = null;
		List<Annonce> listAnnonce = new ArrayList<>();
		if (con != null) {

			try {
				pstm = con.prepareStatement("SELECT * FROM annonce WHERE id_user = ? ");
				pstm.setInt(1, idUser);
				ResultSet res = pstm.executeQuery();
				listAnnonce = resultSetToList(res);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return listAnnonce;
	}

	/**
	 * Pour transformer un resultSet en arraylist
	 * 
	 * @param res
	 * @return
	 */
	private List<Annonce> resultSetToList(ResultSet res) {
		Annonce ann;
		List<Annonce> listAnnonces = new ArrayList<>();
		try {
			while (res.next()) {

				ann = new Annonce();

				ann.setDateAnnonce(res.getDate("date_annonce"));
				ann.setIdAnnonce(res.getInt("id_annonce"));
				ann.setTitre(res.getString("titre"));
				ann.setIsbn(res.getString("isbn"));
				ann.setDateEdition(res.getDate("date_edition"));
				ann.setMaisonEdition(res.getString("maison_edition"));
				ann.setPrixUnitaire(res.getFloat("prix_unitaire"));
				ann.setQuantite(res.getInt("quantite"));
				ann.setRemise(res.getFloat("remise"));
				ann.setIdUser(res.getInt("id_user"));

				listAnnonces.add(ann);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listAnnonces;
	}

	/**
	 * Pour convertire un resultSet en Annonce
	 * 
	 * @param res
	 * @return
	 */
	private Annonce resultSetToAnnonce(ResultSet res) {
		Annonce annonce = null;
		try {
			if (res.next()) {

				annonce = new Annonce();
				annonce.setDateAnnonce(res.getDate("date_annonce"));
				annonce.setIdAnnonce(res.getInt("id_annonce"));
				annonce.setTitre(res.getString("titre"));
				annonce.setIsbn(res.getString("isbn"));
				annonce.setDateEdition(res.getDate("date_edition"));
				annonce.setMaisonEdition(res.getString("maison_edition"));
				annonce.setPrixUnitaire(res.getFloat("prix_unitaire"));
				annonce.setQuantite(res.getInt("quantite"));
				annonce.setRemise(res.getFloat("remise"));
				annonce.setIdUser(res.getInt("id_user"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return annonce;
	}
	

	/**
	 * Pour recuperer une annonce de la base de donn�e en fonction de son id
	 * 
	 * @param idAnnonce
	 * @return
	 */
	public Annonce searchAnnonceParID(int idAnnonce) {
		Connection con = DBConnection.getConnection();
		PreparedStatement pstm = null;
		Annonce annonce = null;
		if (con != null) {

			try {
				pstm = con.prepareStatement("SELECT * FROM annonce WHERE id_annonce = ? ");
				pstm.setInt(1, idAnnonce);
				ResultSet res = pstm.executeQuery();
				annonce = resultSetToAnnonce(res);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return annonce;
	}

	/**
	 * Pour rechercher et recuperer toutes les annonces contien le mot cle dan le
	 * titre
	 * 
	 * @param motCle
	 * @return
	 */
	public List<Annonce> searchAnnonceParTitre(String motCle) {
		Connection con = DBConnection.getConnection();
		PreparedStatement pstm = null;
		List<Annonce> listAnnonce = new ArrayList<>();
		if (con != null) {

			try {
				pstm = con.prepareStatement("SELECT * FROM annonce WHERE titre like ? ");
				pstm.setString(1, "%" + motCle + "%");
				ResultSet res = pstm.executeQuery();
				listAnnonce = resultSetToList(res);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return listAnnonce;
	}

	/**
	 * Pour rechercher un annonce en foction de son ISBN
	 * 
	 * @param isbn
	 * @return
	 */
	public Annonce searchAnnonceParISBN(String isbn) {
		Connection con = DBConnection.getConnection();
		PreparedStatement pstm = null;
		Annonce annonce = null;
		if (con != null) {

			try {
				pstm = con.prepareStatement("SELECT * FROM annonce WHERE isbn = ? ");
				pstm.setString(1, isbn);
				ResultSet res = pstm.executeQuery();
				annonce = resultSetToAnnonce(res);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return annonce;
	}
	
	/**
	 * Pour recuperer toutes les annonce poster dans une ville donn�e
	 * 
	 * @param ville
	 * @return
	 */
	public List<Annonce> searchAnnonceParVille(String ville) {
		Connection con = DBConnection.getConnection();
		PreparedStatement pstm = null;
		List<Annonce> listAnnonce = new ArrayList<>();
		if (con != null) {

			try {
				pstm = con.prepareStatement("SELECT a.* FROM annonce a,utilisateur u "
																+ "	WHERE "
																		+ "a.id_user = u.id_user "
																			+ "AND "
																		+ "u.id_adresse in (select id_adresse from adresse where ville = ?)");
				pstm.setString(1, ville);
				ResultSet res = pstm.executeQuery();
				listAnnonce = resultSetToList(res);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return listAnnonce;
	}
	
}
