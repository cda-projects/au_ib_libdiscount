package fr.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.DBConnection;

/**
 * 
 * @author AU_LibDiscount
 *
 */
public class UtilisateurDAO {

	/**
	 * Pour ajouter un nouveau utilisateur � la base de donn�es
	 * 
	 * @param utilisateur
	 * @return l'utilisateur avec son id g�n�r�
	 */
	public Utilisateur addUtilisateur(Utilisateur utilisateur) {
		Connection con = DBConnection.getConnection();
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement(
						"INSERT INTO Utilisateur (nom,prenom,nom_Librairie,num_tele,email,id_adresse) values (?,?,?,?,?,?)",
						Statement.RETURN_GENERATED_KEYS);
				pstm.setString(1, utilisateur.getNom());
				pstm.setString(2, utilisateur.getPrenom());
				pstm.setString(3, utilisateur.getNomLibrairie());
				pstm.setString(4, utilisateur.getNumTel());
				pstm.setString(5, utilisateur.getEMail());
				pstm.setInt(6, utilisateur.getAdresse().getIdAdresse());
				pstm.executeUpdate();
				ResultSet res = pstm.getGeneratedKeys();

				if (res.next()) {
					utilisateur.setIdUser((res.getInt("id_User")));
					utilisateur.setNom(res.getString("nom"));
					utilisateur.setPrenom(res.getString("prenom"));
					utilisateur.setNomLibrairie(res.getString("nom_librairie"));
					utilisateur.setEMail(res.getString("email"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return utilisateur;
	}

	/**
	 * Pour mettre � jour les infos d'un utilisateur dans la base de donn�es
	 * 
	 * @param utilisateur
	 */
	public void updateUtilisateur(Utilisateur utilisateur) {
		Connection con = DBConnection.getConnection();
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement(
						"UPDATE Utilisateur set nom = ? ,prenom = ? ,nom_Librairie = ? ,num_tele = ? ,email = ? WHERE id_User = ?");
				pstm.setString(1, utilisateur.getNom());
				pstm.setString(2, utilisateur.getPrenom());
				pstm.setString(3, utilisateur.getNomLibrairie());
				pstm.setString(4, utilisateur.getNumTel());
				pstm.setString(5, utilisateur.getEMail());
				pstm.setInt(6, utilisateur.getIdUser());
				pstm.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * Pour recuperer un utilisateur de la base de donn�es en fonction de son id
	 * 
	 * @param idUser
	 * @return
	 */
	public Utilisateur searchUtilisateur(int idUser) {
		Connection con = DBConnection.getConnection();
		Utilisateur utilisateur = null;
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement(
						"SELECT * FROM utilisateur u , adresse a WHERE u.id_adresse = a.id_adresse and id_user = ? ");

				pstm.setInt(1, idUser);

				ResultSet res = pstm.executeQuery();

				if (res.next()) {
					Adresse adresse = new Adresse();
					adresse.setIdAdresse(res.getInt("id_adresse"));
					adresse.setNumero(res.getString("numero"));
					adresse.setLibelle(res.getString("libelle"));
					adresse.setCp(res.getString("cp"));
					adresse.setVille(res.getString("ville"));
					
					utilisateur = new Utilisateur();
					utilisateur.setIdUser((res.getInt("id_User")));
					utilisateur.setNom(res.getString("nom"));
					utilisateur.setPrenom(res.getString("prenom"));
					utilisateur.setNomLibrairie(res.getString("nom_librairie"));
					utilisateur.setEMail(res.getString("email"));
					utilisateur.setAdresse(adresse);
					utilisateur.setNumTel(res.getString("num_tele"));
					
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return utilisateur;
	}

	/**
	 * Pour desactiver un utilisateur
	 * 
	 * @param idUser
	 */
	public void desactiverUtilisateur(int idUser) {
		Connection con = DBConnection.getConnection();
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement("Update utilisateur set compte_active = false WHERE id_user = ? ");
				pstm.setInt(1, idUser);
				pstm.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
