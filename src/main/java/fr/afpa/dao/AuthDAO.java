package fr.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.afpa.beans.Auth;
import fr.afpa.services.DBConnection;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AuthDAO {

	/**
	 * Pour recuperer l'idesntifiant at le mot de passe d'un utilisateur en fonction
	 * de son id
	 * 
	 * @param idUser
	 * @return
	 */
	public Auth getAuth(int idUser) {
		Connection con = DBConnection.getConnection();
		Auth auth = null;
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement("SELECT login,pwd FROM Auth WHERE id_user = ? ");

				pstm.setInt(1, idUser);

				ResultSet res = pstm.executeQuery();

				if (res.next()) {
					auth = new Auth();
					auth.setLogin(res.getString(1));
					auth.setPassword(res.getString(2));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return auth;
	}

	/**
	 * Pour veriffier les info d'autentification
	 * 
	 * @param login
	 * @param password
	 * @return 0 si connecxio echou ,-1 si les info ne sont pas correctes ,-2 si le
	 *         compte est desactiv� ou un nombr > 0 id user
	 */
	public int checkAuth(Auth auth) {
		Connection con = DBConnection.getConnection();
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement("SELECT check_auth(?,?)");

				pstm.setString(1, auth.getLogin());
				pstm.setString(2, auth.getPassword());

				ResultSet res = pstm.executeQuery();

				if (res.next()) {
					return res.getInt(1);
				}

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return 0;
	}

	/**
	 * Pour mettre � jour le mot de passe d'un utilisateur en fonction de son id
	 * 
	 * @param id_user
	 * @param new_password
	 */
	public void updatePassword(int id_user, String new_password) {
		Connection con = DBConnection.getConnection();
		if (con != null) {
			PreparedStatement pstm = null;
			try {

				pstm = con.prepareStatement("UPDATE Auth set pwd = ? WHERE id_User = ?");
				pstm.setString(1, new_password);
				pstm.setInt(2, id_user);
				pstm.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (pstm != null) {
					try {
						pstm.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
