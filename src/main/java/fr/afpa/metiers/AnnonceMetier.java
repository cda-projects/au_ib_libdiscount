package fr.afpa.metiers;

import java.util.List;

import fr.afpa.beans.Annonce;
import fr.afpa.dao.AnnonceDAO;

/**
 * 
 * @author CDA
 *
 */
public class AnnonceMetier {

	private AnnonceDAO dao;

	/**
	 * 
	 */
	public AnnonceMetier() {
		dao = new AnnonceDAO();
	}

	/**
	 * Pour Ajouter une nouvelle annonce
	 * 
	 * @param annonce
	 */
	public void addAnnonce(Annonce annonce) {
		dao.addAnnonce(annonce);
	}

	/**
	 * pour recupperer toutes les annonces
	 * 
	 * @return
	 */
	public List<Annonce> listerAnnonce() {
		return dao.listerAnnonce();
	}

	/**
	 * Pour metre � jour une Annonce
	 * 
	 * @param annonce
	 */
	public void updateAnnonce(Annonce annonce) {
		dao.updateAnnonce(annonce);
	}

	/**
	 * Pour supprimer une annonce en fonction de son id_Annonce
	 * 
	 * @param idAnnonce
	 */
	public void supprimerAnnonce(int idAnnonce) {
		dao.supprimerAnnonce(idAnnonce);
	}

	/**
	 * Pour recupperer toutes les annonce d'un utilisateur donn� en fonction de son
	 * id_user pass� en param
	 * 
	 * @param idUser
	 * @return
	 */
	public List<Annonce> listerMesAnnonces(int idUser) {
		return dao.listerMesAnnonce(idUser);
	}

	/**
	 * recuper une annonce en fonction de son id pass� en param
	 * 
	 * @param idAnnonce
	 * @return
	 */
	public Annonce searchAnnonceParID(int idAnnonce) {
		return dao.searchAnnonceParID(idAnnonce);
	}

	/**
	 * recuper une annonce en fonction de son ISBN
	 * 
	 * @param isbn
	 * @return
	 */
	public Annonce searchAnnonceParISBN(String isbn) {
		return dao.searchAnnonceParISBN(isbn);
	}

	/**
	 * Recupere toute les annonce qui sont poster dan une ville donn�e pass�e en
	 * param
	 * 
	 * @param ville
	 * @return
	 */
	public List<Annonce> searchAnnonceParVille(String ville) {
		return dao.searchAnnonceParVille(ville);
	}

	/**
	 * Pour recupperer les annonce qui contient le mot cle sans son titre
	 * 
	 * @param motCle
	 * @return
	 */
	public List<Annonce> searchAnnonceParTitre(String motCle) {
		return dao.searchAnnonceParTitre(motCle);
	}

}
