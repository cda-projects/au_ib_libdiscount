package fr.afpa.metiers;

import fr.afpa.beans.Adresse;
import fr.afpa.dao.AdresseDAO;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AdresseMetier {
	
	private AdresseDAO dao;
	
	/**
	 * 
	 */
	public AdresseMetier() {
		dao = new AdresseDAO();
	}

	/**
	 * Pour ajouter une nouvelle adresse
	 * 
	 * @param adresse
	 * @return
	 */
	public Adresse addAdresse(Adresse adresse) {
		return dao.addAdresse(adresse);

	}

	/**
	 * Pour recuperer une adresse en fonction de son idAdresse
	 * 
	 * @param idAdresse
	 * @return
	 */
	public Adresse searchAdresse(int idAdresse) {
		return dao.searchAdresse(idAdresse);
	}

	/**
	 * Pour mettre � jour une adresse
	 * 
	 * @param adresse
	 */
	public void updateAdresse(Adresse adresse) {
		dao.updateAdresse(adresse);

	}

}
