package fr.afpa.metiers;

import fr.afpa.beans.Auth;
import fr.afpa.dao.AuthDAO;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AuthMetier {

	/**
	 * Pour recupperer le LOGIN et le PASSWORD d'un utilisateur en fonction de son
	 * id_User
	 * 
	 * @param idUser
	 * @return
	 */
	public Auth getAuth(int idUser) {
		return new AuthDAO().getAuth(idUser);
	}

	/**
	 * Pour verifier si le login et le password sont correctes
	 * 
	 * @param auth
	 * @return
	 */
	public int checkAuth(Auth auth) {
		return new AuthDAO().checkAuth(auth);
	}

}
