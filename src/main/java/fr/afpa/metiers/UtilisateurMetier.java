package fr.afpa.metiers;

import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.UtilisateurDAO;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class UtilisateurMetier {
	
	private UtilisateurDAO dao;
	
	/**
	 * 
	 */
	public UtilisateurMetier() {
		dao = new UtilisateurDAO(); 
	}

	/**
	 * Pour Ajouter un nouveau utilisateur
	 * 
	 * @param utilisateur
	 * @return
	 */
	public Utilisateur addUtilisateur(Utilisateur utilisateur) {
		return dao.addUtilisateur(utilisateur);
	}

	/**
	 * Pour rechercher un utilisateur en fonction de son ID_USER
	 * 
	 * @param idUser
	 * @return
	 */
	public Utilisateur searchUtilisateur(int idUser) {
		return dao.searchUtilisateur(idUser);
	}

	/**
	 * Pour mettre � jour l'utilisateur passer an parametre dans la base de donn�es
	 * 
	 * @param user
	 */
	public void updateUtilisateur(Utilisateur user) {
		dao.updateUtilisateur(user);
	}

	/**
	 * Pour desactiver un utilisateur en fonction de son idUser
	 * 
	 * @param idUser
	 */
	public void desactiverUtilisateur(int idUser) {
		dao.desactiverUtilisateur(idUser);
	}

}
