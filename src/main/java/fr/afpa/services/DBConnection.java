package fr.afpa.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 
 * @author AU_LibDiscount
 *
 */
public interface DBConnection {

	/**
	 * Pour creer une instance CONNECTION avec la base de donn�es
	 * 
	 * @return une instance de connection avec la base de donn�e
	 */
	public static Connection getConnection() {
		try {
			String url = "jdbc:postgresql://localhost:5432/LibDiscount";
			Properties props = new Properties();
			props.setProperty("user", "cda");
			props.setProperty("password", "cda");
			return DriverManager.getConnection(url, props);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
