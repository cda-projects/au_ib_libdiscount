package fr.afpa.controles;

import fr.afpa.beans.Adresse;
import fr.afpa.metiers.AdresseMetier;

public class AdresseControle {
	
	private AdresseMetier metier;
	
	public AdresseControle() {
		metier = new AdresseMetier();
	}

	/**
	 * Pour verifier le format de NUMERO
	 * 
	 * @param numero
	 * @return
	 */
	public boolean checkNumero(String numero) {
		return numero != null && numero.length() <= 50 && numero.matches("^[0-9]{1,}[a-zA-A0-9\\s]*");
	}

	/**
	 * Pourverifier le format de LIBELLE
	 * 
	 * @param libelle
	 * @return
	 */
	public boolean checkLibelle(String libelle) {
		return libelle != null && libelle.length() <= 150 && libelle.matches("[0-9a-zA-Z,-\\.\\s]+");
	}

	/**
	 * Pour verifier le format de CODE POSTALE
	 * 
	 * @param cp
	 * @return
	 */
	public boolean checkCP(String cp) {
		return cp != null && cp.length() == 5 && cp.matches("[0-9]{5}");
	}

	/**
	 * Pour verifier le format de VILLE
	 * 
	 * @param ville
	 * @return
	 */
	public boolean checkVille(String ville) {
		return ville != null && ville.length() <= 30 && ville.matches("[a-zA-Z-\\s]+");
	}

	/**
	 * Pour Creer et ajouter une nouvelle adresse
	 * 
	 * @param numero
	 * @param libelle
	 * @param cp
	 * @param ville
	 * @return
	 */
	public Adresse addAdresse(String numero, String libelle, String cp, String ville) {
		Adresse adresse = new Adresse();
		adresse.setNumero(numero);
		adresse.setLibelle(libelle);
		adresse.setCp(cp);
		adresse.setVille(ville);
		return metier.addAdresse(adresse);
	}

	/**
	 * Pour recupperer une adresse en fonction de son ID_Adresse
	 * 
	 * @param idAdresse
	 * @return
	 */
	public Adresse searchAdresse(int idAdresse) {
		return metier.searchAdresse(idAdresse);
	}

	/**
	 * Pour metter � jour une adresse
	 * 
	 * @param adresse
	 */
	public void updateAdresse(Adresse adresse) {
		metier.updateAdresse(adresse);

	}

}
