package fr.afpa.controles;

import fr.afpa.beans.Auth;
import fr.afpa.metiers.AuthMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AuthControle {

	/**
	 * Pour verifier le format de LOGIN
	 * 
	 * @param login
	 * @return
	 */
	public boolean checkLogin(String login) {
		return login != null && login.length() == 10 && login.matches("[0-9]{10}");
	}

	/**
	 * Pour verifier le format de PASSWORD
	 * 
	 * @param login
	 * @return
	 */
	public boolean checkPassword(String password) {
		return password != null && password.length() >= 8;
	}

	/**
	 * Pour recupperer l'auth d'un utilisateur en fonction de son idUser
	 * 
	 * @param idUser
	 * @return
	 */
	public Auth getAuth(int idUser) {
		return new AuthMetier().getAuth(idUser);
	}

	/**
	 * Pour verifier si le LOGIN et le PASSWORD sont correctes
	 * 
	 * @param login
	 * @param password
	 * @return
	 */
	public int checkAuth(String login, String password) {
		return new AuthMetier().checkAuth(new Auth(login, password));
	}

}
