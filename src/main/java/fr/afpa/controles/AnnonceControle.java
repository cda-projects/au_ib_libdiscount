package fr.afpa.controles;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import fr.afpa.beans.Annonce;
import fr.afpa.metiers.AnnonceMetier;

/**
 * 
 * @author CDA
 *
 */
public class AnnonceControle {

	private AnnonceMetier metier;

	/**
	 * 
	 */
	public AnnonceControle() {
		metier = new AnnonceMetier();
	}

	/**
	 * Pour verifier le format de TITRE
	 * 
	 * @param titre
	 * @return
	 */
	public boolean checkTitre_MaisonEdition(String titre) {
		return titre != null && titre.length() <= 50 && titre.matches("[a-zA-Z1-9-\\.\\s]+");
	}

	/**
	 * Pour verifier le format de ISBN
	 * 
	 * @param isbn
	 * @return
	 */
	public boolean checkISBN(String isbn) {
		return isbn != null && isbn.length() <= 13 && isbn.length() >= 11 && isbn.matches("[0-9]{11,13}");
	}

	/**
	 * Pour verifier le format de DATE_EDITION
	 * 
	 * @param date
	 * @return
	 */
	public boolean checkDateEdition(String date) {
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			df.setLenient(false);
			try {
				df.parse(date);
			} catch (ParseException e) {
				return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Pour verifier le format de QUANTITE
	 * 
	 * @param qte
	 * @return
	 */
	public boolean checkQuantite(String qte) {
		return qte != null && qte.matches("[1-9][0-9]*");
	}

	/**
	 * Pour verifier le format de REMISE
	 * 
	 * @param remise
	 * @return
	 */
	public boolean checkRemise(String remise) {
		return remise != null && (remise.matches("[0-9]{1,2}(\\.[0-9]*)?") || remise.matches("100(\\.[0]*)?"));
	}

	/**
	 * Pour verifier le format de PRIX_UNITAIRE
	 * 
	 * @param prix
	 * @return
	 */
	public boolean checkPrixUnitaire(String prix) {
		return prix != null && (prix.matches("[1-9][0-9]*(\\.[0-9]*)?") || prix.matches("0(\\.[0-9]*)?"));
	}

	/**
	 * Pour verifiier si une annonce appartient � un utilisateur donn� ou pas en
	 * fonction de l'idAnnonce et l'idUser
	 * 
	 * @param idUser
	 * @param idAnnonce
	 * @return
	 */
	public boolean checkAppartenance(int idUser, String idAnnonce) {
		Annonce annonce = searchAnnonceParID(idAnnonce);
		return annonce != null && annonce.getIdUser() == idUser;
	}

	/**
	 * Pour verifier le format de ID_ANNONCE
	 * 
	 * @param idAnnonce
	 * @return
	 */
	public boolean checkIDAnnonce(String idAnnonce) {
		return idAnnonce != null && idAnnonce.matches("[0-9]+");
	}

	/**
	 * Pour lister toutes les annonces poster par tous les utilisateur
	 * 
	 * @return
	 */
	public List<Annonce> listerAnnonce() {
		return metier.listerAnnonce();
	}

	/**
	 * Pour lister toutes les annonce poster par un utilisateur en fonction de son
	 * id_user
	 * 
	 * @param idUser
	 * @return
	 */
	public List<Annonce> listerMesAnnonce(int idUser) {
		return metier.listerMesAnnonces(idUser);
	}

	/**
	 * Pour Creer une nouvelle annonce et la rajoter a la base de donn�es
	 * 
	 * @param titre
	 * @param isbn
	 * @param dateEdition
	 * @param quantite
	 * @param remise
	 * @param prixUnitaire
	 * @param maisonEdition
	 * @param idUser
	 */
	public void addAnnonce(String titre, String isbn, String dateEdition, String quantite, String remise,
			String prixUnitaire, String maisonEdition, int idUser) {
		Annonce annonce = new Annonce();
		annonce.setTitre(titre);
		annonce.setIsbn(isbn);
		annonce.setDateEdition(Date.valueOf(dateEdition));
		annonce.setQuantite(Integer.parseInt(quantite));
		annonce.setRemise(Float.parseFloat(remise));
		annonce.setPrixUnitaire(Float.parseFloat(prixUnitaire));
		annonce.setMaisonEdition(maisonEdition);
		annonce.setIdUser(idUser);

		metier.addAnnonce(annonce);
	}

	/**
	 * Pour recupperer une annonce en fonction de son ID
	 * 
	 * @param idAnnonce
	 * @return
	 */
	public Annonce searchAnnonceParID(String idAnnonceStr) {
		int idAnnonce = Integer.parseInt(idAnnonceStr);
		if (idAnnonce > 0) {
			return metier.searchAnnonceParID(idAnnonce);
		}
		return null;
	}

	/**
	 * Pour recuperer une annononce en fonction de son ISBN
	 * 
	 * @param isbn
	 * @return
	 */
	public Annonce searchAnnonceParISBN(String isbn) {
		return metier.searchAnnonceParISBN(isbn);
	}

	/**
	 * Pour recuperer toutes les annononces poster dans une ville donn�e en param
	 * 
	 * @param ville
	 * @return
	 */
	public List<Annonce> searchAnnonceParVille(String ville) {
		return metier.searchAnnonceParVille(ville.toUpperCase());
	}

	/**
	 * Pour recuperer toutes les annononces contien le mot cle dans son titre
	 * 
	 * @param motCle
	 * @return
	 */
	public List<Annonce> searchAnnonceParTitre(String motCle) {
		return metier.searchAnnonceParTitre(motCle.toUpperCase());
	}

	/**
	 * Pour mettre a jour les infos d'une annonce
	 * 
	 * @param annonce
	 */
	public void updateAnnonce(Annonce annonce) {
		metier.updateAnnonce(annonce);
	}

	/**
	 * Pour supprimer une annonce
	 * 
	 * @param idAnnonce
	 */
	public void supprimerAnnonce(String idAnnonce) {
		metier.supprimerAnnonce(Integer.parseInt(idAnnonce));
	}
}
