package fr.afpa.vues;

import java.sql.Date;
import java.util.List;
import java.util.Scanner;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Auth;
import fr.afpa.beans.Utilisateur;
import fr.afpa.controles.AdresseControle;
import fr.afpa.controles.AnnonceControle;
import fr.afpa.controles.AuthControle;
import fr.afpa.controles.UtilisateurControle;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class Menus {

	private Scanner in;
	private UtilisateurControle uc;
	private AdresseControle ac;
	private AnnonceControle anc;
	private AnnonceFields anf;
	private AdresseFields af;
	private UserFields uf;

/******************************************************************************************************/
	
	/**
	 * 
	 */
	public Menus() {
		in = new Scanner(System.in);
		uc = new UtilisateurControle();
		ac = new AdresseControle();
		anc = new AnnonceControle();
		anf = new AnnonceFields(in, anc);
		af = new AdresseFields(in, ac);
		uf = new UserFields(in, uc);
	}
	
/******************************************************************************************************/
	
	/**
	 * Pour Afficher le menu principale
	 */
	public void getAccueil() {
		String s = ""+
				"<><><><><><><><><><><><><><><><><><>\n"+
				"<>            Accueil             <>\n"+
				"<>                                <>\n"+
				"<>      <1> Se Connecter          <>\n"+
				"<>      <2> S'inscrire            <>\n"+
				"<>      <0> Quitter               <>\n"+
				"<>                                <>\n"+
				"<><><><><><><><><><><><><><><><><><>\n";
		
		String choix = "";
		do {
			System.out.println(s);
			System.out.print("Entrer votre choix >> ");
			choix = in.nextLine();
			switch (choix) {
			case "1":
				connecter();
				break;
			case "2":
				inscrire();
				break;
			case "0":
				in.close();
				System.exit(0);
				break;
			default:
				System.out.println("Mauvais choix.");
				break;
			}
		} while (!choix.equals("0"));
	}
	
/******************************************************************************************************/	
	
	/**
	 * Pour afficher la page connection
	 */
	private void connecter() {
		AuthControle auc = new AuthControle();
		AuthFields af = new AuthFields(in, auc);
		
		System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><>\n"+
						   "<>                    Connexion                   <>\n");
		
		String login = af.loginField("Saissisez votre login >> ");
		String password = af.passwordField("Saissisez voutre password >> ");
		
		int check = auc.checkAuth(login,password);
		if(check == -1) {
			System.out.println("Login ou password sont incorrectes.");
		}else if(check == -2) {
			System.out.println("Votre compte est desactiv�.");
		}else {
			System.out.println("Bienvenu");
			getEspaceLibraire(check);
		}
	}

/******************************************************************************************************/
	
	/**
	 * Pour afficher le formullaire d'inscription
	 */
	private void inscrire() {
		System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><>\n"
						 + "<>                 Formulaire d'inscription       <>\n");

		String nom = uf.nomField("Saisissez votre nom >> ");
		String prenom = uf.prenomField("Saisissez votre prenom >> ");
		String nomLibrairie = uf.nomLibrairieField("Saisissez le nom de Librairie >> ");
		String numTel = uf.numTelField("Saisissez votre numero de telephone ex 0700000000 >> ");
		String email = uf.emailField("Saisissez votre email >> ");

		Adresse adresse = createAdresseFormulaire();
		if (adresse != null && adresse.getIdAdresse() != 0) {
			
			Utilisateur utilisateur = uc.addUtilisateur(nom, prenom, nomLibrairie, numTel, email,adresse);
			
			if (utilisateur != null && utilisateur.getIdUser() != 0) {
				
				Auth auth = new AuthControle().getAuth(utilisateur.getIdUser());
				
				if (auth != null) {
					
					System.out.println(utilisateur+"\n"+adresse+"\n"+auth);
				
				}
			}
		}
	}
	
/******************************************************************************************************/
	/**
	 * Pour afficher le formulaire de creation d'Adresse
	 * 
	 * @return
	 */
	private Adresse createAdresseFormulaire() {
		System.out.println("Saisissez votre adresse >> ");

		String numero = af.numeroField("Saisissez le Numero de rue >> ");
		String libelle = af.libelleField("Saisissez le libelle de rue >> ");
		String cp = af.cpField("Saisissez le code postale >> ");
		String ville = af.villeField("Saisissez la ville >> ");

		return ac.addAdresse(numero, libelle, cp, ville);
	}

/******************************************************************************************************/	
	
	/**
	 * Pour Afficher l'espace libraire
	 * 
	 * @param idUser
	 */
	private void getEspaceLibraire(int idUser) {
		
		Utilisateur user = uc.searchUtilisateur(idUser);
		
		if(user != null) {
			String s = ""+
					"<><><><><><><><><><><><><><><><><><><><><><><><><><>\n"+
					"                  "+user.getNomLibrairie()+"\n"+
					"            		"+user.getNom()+" "+user.getPrenom()+"\n"+
					"                   Espace libraire\n"+
					"       <><><><><><><><><><><><><><><><><><>\n"+
					"       <>            Dashboard           <>\n"+
					"       <>                                <>\n"+
					"       <>      <1> Afficher Infos        <>\n"+
					"       <>      <2> Modifier Infos        <>\n"+
					"       <>      <3> Modifier Adresse      <>\n"+
					"       <>      <4> Gerer Annonces        <>\n"+
					"       <>      <5> Se d�sinscrire        <>\n"+
					"       <>      <0> Se d�connecter        <>\n"+
					"       <>                                <>\n"+
					"       <><><><><><><><><><><><><><><><><><>\n";
			
			String choix = "";
			do {
				System.out.println(s);
				System.out.print("Saisissez votre choix >> ");
				choix = in.nextLine();
				switch (choix) {
				case "1":
					afficherInfoUser(user);
					break;
				case "2":
					modifierInfo(user);
					break;
				case "3":
					modifierAdresse(user.getAdresse());
					break;
				case "4":
					getMenuGestionAnnonce(user);
					break;
				case "5":
					desinscrire(idUser);
					break;
				case "0":
					System.out.println();
					break;
				default:
					System.out.println("Mauvais choix.");
					break;
				}
			} while (!choix.equals("0"));
		}
	}
	
/******************************************************************************************************/

	private void desinscrire(int idUser) {
		System.out.print("Voulez vous se d�sinscrire (1 pour valider autre si non) >>");
		String choix = in.nextLine();
		if("1".equalsIgnoreCase(choix.replaceAll(" ", ""))){
			uc.desactiverUtilisateur(idUser);
			System.out.println("Votre compte est bien d�sactiv�!");
			getAccueil();
		}
	}
	
/******************************************************************************************************/
	
	/**
	 * Pour Afficher infos User
	 * 
	 * @param user
	 */
	private void afficherInfoUser(Utilisateur user) {
		System.out.println("=============================================");
		System.out.println(user);
		System.out.println("=============================================");
	}
	
/******************************************************************************************************/	
	
	/**
	 * Pour Afficher le formulaire de modification d'info user
	 * 
	 * @param idUser
	 */
	private void modifierInfo(Utilisateur user) {
		if (user != null) {

			if (uf.questionField("votre nom")) {
				user.setNom(uf.nomField("Saisissez votre nouveau nom >> "));
			}

			if (uf.questionField("votre prenom")) {
				user.setPrenom(uf.prenomField("Saisissez votre nouveau prenom >> "));
			}

			if (uf.questionField("votre nom librairie")) {
				user.setNomLibrairie(uf.nomLibrairieField("Saisissez votre nouveau nom librairie >> "));
			}

			if (uf.questionField("votre email")) {
				user.setEMail(uf.emailField("Saisissez votre nouveau email >> "));
			}

			if (uf.questionField("votre numero de telephone")) {
				user.setNumTel(uf.numTelField("Saisissez votre nouveau numero de telephone >> "));
			}

			uc.updateUtilisateur(user);
		}
	}
/******************************************************************************************************/
	
	/**
	 * Pour Afficher le formuler de modification d'info Adresse
	 * 
	 * @param adresse
	 */
	private void modifierAdresse(Adresse adresse) {		
		if (adresse != null) {
			if (af.questionField("le numero de rue")) {
				adresse.setNumero(af.numeroField("Saisissez le noveau Numero de rue >>"));
			}

			if (af.questionField("le libelle")) {
				adresse.setLibelle(af.libelleField("Saisissez le nouveau libelle de rue >> "));
			}

			if (af.questionField("le code postale")) {
				adresse.setCp(af.cpField("Saisissez le nouveau code postale >> "));
			}

			if (af.questionField("la ville")) {
				adresse.setVille(af.villeField("Saisissez la nouvelle ville >> "));
			}

			ac.updateAdresse(adresse);
		}
		
	}
	
/******************************************************************************************************/
	
	/**
	 * Pour Afficher le menu de gestion d'ANNONCES
	 * 
	 * @param user
	 */
	private void getMenuGestionAnnonce(Utilisateur user) {
		String s = "\n"+
				"       <><><><><><><><><><><><><><><><><><><><><><><><>\n"+
				"       <>              Gestion Annonces              <>\n"+
				"       <>                                            <>\n"+
				"       <>      <1> Poster Annonce                    <>\n"+
				"       <>      <2> Modifier Annonce                  <>\n"+
				"       <>      <3> Suprimmer Annonce                 <>\n"+
				"       <>      <4> Lister mes annonces               <>\n"+
				"       <>      <5> Lister toutes les Annonces        <>\n"+
				"       <>      <6> Rechercher Annonce par Titre      <>\n"+
				"       <>      <7> Rechercher Annonce par ISBN       <>\n"+
				"       <>      <8> Rechercher Annonce par Ville      <>\n"+
				"       <>      <0> Dashboard                         <>\n"+
				"       <>                                            <>\n"+
				"       <><><><><><><><><><><><><><><><><><><><><><><><>\n";
		
		String choix = "";
		
		do {
			System.out.println(s);
			System.out.print("Saisissez votre choix >> ");
			choix = in.nextLine();
			
			switch (choix) {
				case "1":
					posterAnnonce(user);
					break;
				case "2":
					modifierAnnonce(user);
					break;
				case "3":
					supprimerAnnonce(user);
					break;
				case "4":
					listerMesAnnonces(user);
					break;
				case "5":
					listerToutesLesAnnonces();
					break;
				case "6":
					searchAnnonceParTitre();
					break;
				case "7":
					searchAnnonceParISBN();
					break;
				case "8":
					searchAnnonceParVille();
					break;
				case "0":
					System.out.println();
					break;
	
				default:
					System.out.println("Mauvais choix.");
					break;
			}
		}while(!"0".equals(choix));	
	}
	
/******************************************************************************************************/	
	
	/**
	 * Pour Afficher la liste des annonces d'un utilisateur
	 * 
	 * @param user
	 */
	private void listerMesAnnonces(Utilisateur user) {
		System.out.println("<<<<<<<<<<<<< MES ANNONCES >>>>>>>>>>>>>");
		List<Annonce> mesAnnonces = anc.listerMesAnnonce(user.getIdUser());
		if(mesAnnonces == null || mesAnnonces.isEmpty()) {
			System.err.println("Vous n'avesz pad des annonces Post�es !");
			System.out.println();
		} else {
			for(Annonce a : mesAnnonces) {
				System.out.println(a+"\n\n");
			}
		}
		
	}
	
/******************************************************************************************************/
	
	/**
	 * Pour afficher la liste de toutes les annonces
	 */
	private void listerToutesLesAnnonces() {
		System.out.println("<<<<<<<<<<<<< TOUTES LES ANNONCES >>>>>>>>>>>>>");
		List<Annonce> listAnnonces = anc.listerAnnonce();
		if(listAnnonces == null || listAnnonces.isEmpty()) {
			System.err.println("Il n'y a pas encore des annonces post�es !");
			System.out.println();
		} else {
			for(Annonce a : listAnnonces) {
				printAnnonce(a);
			}
		}
	}
	
/******************************************************************************************************/
	
	/**
	 * Pour Afficher le formulaire de creation d'annonce
	 * 
	 * @param user
	 */
	private void posterAnnonce(Utilisateur user) {
		System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><>\n"
						 + "<>                Formulaire Annonce              <>\n");

		String titre = anf.titre_MaisonEdition_Field("Saisissez le titre >> ");
		String maisonEdition = anf.titre_MaisonEdition_Field("Saissisez le nom de la Maison Edition >> ");
		String dateEdition = anf.dateEditionField("Saisissez la date D'edition (ex: YYYY-MM-DD) >>  ");
		String isbn = anf.isbnField("Saisissez le ISBN >> ");
		String prixUnitaire = anf.prixUnitaireField("Saisissez le prix unitaire >> ");
		String quantite = anf.quantiteField("Saissisez le quantit� >> ");
		String remise = anf.remiseField("Saisissez le taux de remise en % >> ");

		anc.addAnnonce(titre, isbn, dateEdition, quantite, remise, prixUnitaire, maisonEdition, user.getIdUser());

	}

/******************************************************************************************************/
	
	/**
	 * PourAfficher le formulaire de modification d'annonce
	 * 
	 * @param user
	 */
	private void modifierAnnonce(Utilisateur user) {
		System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><>\n"
						 + "<>          Formulaire Modification Annonce       <>\n");

		
		String idAnnonce = anf.idAnnonceField("Saisissez l'ID Annonce � modifier >> ");
	
		if (anc.checkAppartenance(user.getIdUser(), idAnnonce)) {
		
			Annonce annonce = anc.searchAnnonceParID(idAnnonce);
		
			if (annonce != null) {
				if (anf.questionField("le titre")) {
					annonce.setTitre(anf.titre_MaisonEdition_Field("Saisissez le nouveau titre >> "));
				}
				if (anf.questionField("l'ISBN")) {
					annonce.setIsbn(anf.isbnField("Saisissez le nouveau ISBN >> "));
				}
				if (anf.questionField("la Maison d'�dition")) {
					annonce.setMaisonEdition(
							anf.titre_MaisonEdition_Field("Saissisez le nouveau nom de la Maison Edition >> "));
				}
				if (anf.questionField("la date d'�dition")) {
					annonce.setDateEdition(Date.valueOf(
							anf.dateEditionField("Saisissez la nouvelle date D'edition (ex: YYYY-MM-DD) >>  ")));
				}
				if (anf.questionField("le prix unitaire")) {
					annonce.setPrixUnitaire(
							Float.parseFloat(anf.prixUnitaireField("Saisissez le nouveau prix unitaire >> ")));
				}
				if (anf.questionField("le quantit�")) {
					annonce.setQuantite(Integer.parseInt(anf.quantiteField("Saissisez la nouvelle quantit� >> ")));
				}
				if (anf.questionField("la remise")) {
					annonce.setRemise(
							Float.parseFloat(anf.remiseField("Saisissez le nouveau taux de remise en % >> ")));
				}
				anc.updateAnnonce(annonce);
			}
		} else {
			System.err.println("ERROR annonce non trouvalble ou elle ne vous appartient pas.");
			System.out.println();
		}
	}

/******************************************************************************************************/
	
	/**
	 * Pour Supprimer une annonce 
	 * @param user
	 */
	private void supprimerAnnonce(Utilisateur user) {
		String idAnnonce = anf.idAnnonceField("Saisissez l'ID Annonce � supprimer >> ");
	
		if (anc.checkAppartenance(user.getIdUser(), idAnnonce)) {
			anc.supprimerAnnonce(idAnnonce);
			System.out.println("l'annonce a �t� bien supprim�e");
		} else {
			System.err.println("ERROR annonce non trouvalble ou elle ne vous appartient pas.");
			System.out.println();
		}
	}
	
/******************************************************************************************************/

	/**
	 * Pour rechercher une annonce avec un mot cle de titre
	 * 
	 */
	private void searchAnnonceParTitre() {
		String motCle = anf.titre_MaisonEdition_Field("Saisissez un mot cl� pour rechercher >> ");
		List<Annonce> listAnnonce = anc.searchAnnonceParTitre(motCle);
		if(listAnnonce != null && !listAnnonce.isEmpty()) {
			for(Annonce a : listAnnonce) {
				printAnnonce(a);
			}
		} else {
			System.out.println("D�sol�, Il n'y a pas des resultats.");
		}
		
	}
	
/******************************************************************************************************/
	
	/**
	 * Pour rechercher des annonce en fonction de leur ville
	 */
	private void searchAnnonceParVille() {
		String ville = af.villeField("Saisissez une ville pour rechercher >> ");
		List<Annonce> listAnnonce = anc.searchAnnonceParVille(ville);
		if(listAnnonce != null && !listAnnonce.isEmpty()) {
			for(Annonce a : listAnnonce) {
				printAnnonce(a);
			}
		} else {
			System.out.println("D�sol�, Il n'y a pas des resultats.");
		}
		
	}
	
/******************************************************************************************************/	
	
	/**
	 * Pour rechercher une annonce en foction de son ISBN
	 */
	private void searchAnnonceParISBN() {
		String isbn = anf.isbnField("Saisissez un ISBN pour rechercher >> ");
		Annonce annonce = anc.searchAnnonceParISBN(isbn);
		if(annonce != null ) {
			printAnnonce(annonce);
		} else {
			System.out.println("D�sol�, Il n'y a pas des resultats.");
		}
		
	}
	
/******************************************************************************************************/
	
	/**
	 * Pour afficher les infos d'une annonce passer en param 
	 * @param annonce
	 */
	private void printAnnonce(Annonce annonce) {
		if(annonce != null) {
			Utilisateur user = uc.searchUtilisateur(annonce.getIdUser());
			System.out.println("=========================================================");
			System.out.println("====INFO LIBRAIRE :");
			System.out.println(user);
			System.out.println("====INFO ANNONNCE :");
			System.out.println(annonce);
			System.out.println("=========================================================");
			
		}
	}
	
/******************************************************************************************************/	

}



	



