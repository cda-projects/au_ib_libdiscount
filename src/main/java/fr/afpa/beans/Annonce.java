package fr.afpa.beans;

import java.sql.Date;
import java.text.DecimalFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class Annonce {

	private int idAnnonce;
	private int idUser;
	private String maisonEdition;
	private String titre;
	private Date dateAnnonce;
	private Date dateEdition;
	private String isbn;
	private float prixUnitaire;
	private int quantite;
	private float remise;

	/**
	 * 
	 */
	public String  toString() {
		DecimalFormat df = new DecimalFormat(".##");
		return "   ID ANNONCE : "+idAnnonce+"\n"+
			   "   DATE-ANNONCE : "+dateAnnonce+"\n"+
			   "   TITRE : "+titre+"\n"+
			   "   MAISON-EDITION : "+maisonEdition+"\n"+
			   "   DATE-EDITION : "+dateEdition+"\n"+
			   "   ISBN : "+isbn+"\n"+
			   "   PRIX-UNITAIRE : "+prixUnitaire+" euros\n"+
			   "   QUANTITE : "+quantite+"\n"+
			   "   REMISE : "+remise+"%\n"+
			   "   PRIX-TOTALE : "+df.format(prixUnitaire*quantite*(1-(remise/100)))+" euros";		   
	}
}
