package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author CDA
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Auth {

	private String login;
	private String password;

	/**
	 * 
	 */
	public String toString() {
		return "LOGIN : " + login + " , PASSWORD : " + password;
	}
}
