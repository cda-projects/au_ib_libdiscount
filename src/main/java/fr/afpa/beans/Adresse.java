package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author CDA
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Adresse {

	private int idAdresse;
	private String numero;
	private String libelle;
	private String cp;
	private String ville;

	/**
	 *
	 */
	public String toString() {
		return "       >> adresse : " + numero + " " + libelle + " ," + cp + " " + ville;
	}
}
