package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author CDA
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Utilisateur {
	private int idUser;
	private Adresse adresse;
	private String nom;
	private String prenom;
	private String eMail;
	private String numTel;
	private String nomLibrairie;
	
	/**
	 * 
	 */
	public String toString() {
		return "infos:\n"+
			   "       >> nom :"+nom+"\n"+
			   "       >> prenom :"+prenom+"\n"+
			   "       >> nomLibrairie :"+nomLibrairie+"\n"+
			   "       >> email :"+eMail+"\n"+
			   "       >> numero telephone :"+numTel+"\n"+
			   adresse+"\n";		
	}
}
